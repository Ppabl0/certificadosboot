# Información del proyecto

Proyecto basado en otro proyecto sin springboot

### Detalles del proyecto
Lista de puntos elegidos arbitrariamente o reusados porque en el proyecto no los especifica.
Mi idea es quitar agregar o modificarlos segun lo hablemos con el mentor

- Nombre de tags es unico, no pueden existir 2 tags con el mismo nombre
- Nombre de certificados de regalo es unico no deben existir 2 certificados con el mismo nombre
- La descripción del certificado es opcional
- La fecha de creación y de ultima modificación se crean automaticamente, api no permite modificarlos
- Al crear un certificado el atributo "price" (saldo) no debe ser menor a 1
- Los tags representan categorias
- Base de datos debe permitir agregar tags con el mismo nombre, la duplicacion se evita desde el codigo java
- Un certificado solo puede pertenecer a 1 usuario

***

### (DontREADME)
Notas personales del creador de este repo

##### Añadir
- ¿mayusculas y minusculas
- Website UI
  - search
- Tests
  - Tag service unit tests 
- jar mvc plugin sauz coud
- react*

##### Añadidos
- dto
- quitar dto
- interfaz repositorio
- excepciones
- quitar excepciones
- composite key
- quitar composite key
- jackson ignore
- lambak
- properties
- tabla usuarios
  - tabla a database
  - columna usuario_id a certificados
  - API seccion user
- pk a tabla gifttag
- Spring skiurity
  - login
  - roles
  - tabla authorities
  - bcrypt password
- Website UI
  - tarjetas
  - thymeleaf boostrap
  - navbar
  - logout login boton
  - home
  - admin tablas
- AWS training
- jenkins
- Tests
  - GiftCert service unit tests 
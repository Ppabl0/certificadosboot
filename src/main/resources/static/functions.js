
function agregar(){

   let name = document.getElementById("name").value;
   let description = document.getElementById("description").value;
   let price = Number(document.getElementById("price").value)
   let duration = Number(document.getElementById("duration").value)
   let owner = document.getElementById("owner").value


   data = {}
   data.owner = owner
   data.giftCertName = name
   data.price = Number(price)
   data.duration = Number(duration)

   if(description != ''){
       data.description = description
   }



   fetch("http://localhost:8080/api/giftcerts", {
       method: "POST",
       body: JSON.stringify(data),
       headers: {"Content-Type":"application/json"}
   })
   .then((res) => res.json)
   .then((data) => {window.alert("Gift creado con exito")})
   .catch((error) => {
       console.error("error", error);
       });
    
}


function activateCreate(element){
    //desactivar boton y activar los otros

    element.disabled = true
    document.getElementById("delete-btn").disabled = false
    document.getElementById("modify-btn").disabled = false

    
    //ACTIVOS
    //inputs
    document.getElementById("name").disable = false
    document.getElementById("description").disable = false
    document.getElementById("price").disable = false
    document.getElementById("duration").disable = false
    document.getElementById("owner").disable = false
    
    //labels
    document.getElementById("name-label").hidden = false
    document.getElementById("description-label").hidden = false
    document.getElementById("price-label").hidden = false
    document.getElementById("duration-label").hidden = false
    document.getElementById("owner-label").hidden = false
    
    
    //INACTIVOS
    document.getElementById("id").disable = true
    document.getElementById("id-label").hidden = true

    //submit funcion
    document.getElementById("submit").setAttribute("onclick", "agregar()")
}


function activateDelete(element){
    document.getElementById("create-btn").disabled = false
    document.getElementById("modify-btn").disabled = false
    element.disabled = true

    
    //ACTIVOS
    document.getElementById("id").disable = false
    document.getElementById("id-label").hidden = false
    
    
    //INACTIVOS
    //inputs
    document.getElementById("name").disable = true
    document.getElementById("description").disable = true
    document.getElementById("price").disable = true
    document.getElementById("duration").disable = true
    document.getElementById("owner").disable = true
    //labels
    document.getElementById("name-label").hidden = true
    document.getElementById("description-label").hidden = true
    document.getElementById("price-label").hidden = true
    document.getElementById("duration-label").hidden = true
    document.getElementById("owner-label").hidden = true

    //submit
    document.getElementById("submit").setAttribute("onclick", "borrar()")
}


function activateModify(element){
    document.getElementById("create-btn").disabled = false
    document.getElementById("delete-btn").disabled = false
    element.disabled = true

    //ACTIVOS
    document.getElementById("id-label").hidden = false
    document.getElementById("name-label").hidden = false
    document.getElementById("description-label").hidden = false
    document.getElementById("price-label").hidden = false
    document.getElementById("duration-label").hidden = false
    document.getElementById("owner-label").hidden = false


    document.getElementById("id").disable = false
    document.getElementById("name").disable = false
    document.getElementById("description").disable = false
    document.getElementById("price").disable = false
    document.getElementById("duration").disable = false
    document.getElementById("owner").disable = false

    
    //submit
    document.getElementById("submit").setAttribute("onclick", "modificar()")
}

//delete on table
function deleteGift(element){
    //sacar id
    let id = element.getAttribute("data-giftid")
    
    
    fetch(`http://localhost:8080/api/giftcerts/${id}`, {
        method: "DELETE",
       headers: {"Content-Type":"application/json"}
   })
   .then(window.alert("Certificado borrado"))
   .catch((error) => {
       console.error("error", error);
       });
    location.reload()
    
}


function deleteTag(element){
    //sacar id
    let id = element.getAttribute("data-tagid")
    
    
    fetch(`http://localhost:8080/api/tags/${id}`, {
        method: "DELETE",
       headers: {"Content-Type":"application/json"}
   })
   .then(window.alert("Tag borrado"))
   .catch((error) => {
       console.error("error", error);
       });
}



function modifyGift(){
   let id = document.getElementById("id").value
   let owner = document.getElementById("owner").value
   let name = document.getElementById("name").value
   let description = document.getElementById("description").value
   let price = document.getElementById("price").value
   let duration = document.getElementById("duration").value


    data = {}
    
    if(owner != ''){
        data.owner = owner
    }
    if(name != ''){
        data.giftCertName = name
    }
    if(description != ''){
        data.description = description
    }
    if(price != ''){
        data.price = Number(price)
    }
    if(duration != ''){
        data.duration = Number(duration)
    }


    fetch(`http://localhost:8080/api/giftcerts/${id}`, {
        method: "PATCH",
        body: JSON.stringify(data),
       headers: {"Content-Type":"application/json"}
   })
   .then(showAlert("Gift certificado Modificado"))
   .catch((error) => {
       console.error("error", error);
       });


}

function preFill(element){
    let row = element.parentElement.parentElement

    document.getElementById("id").value = row.cells.item(0).innerText; //ID
    document.getElementById("owner").setAttribute("placeholder", row.cells.item(1).innerText) //owner
    document.getElementById("name").setAttribute("placeholder", row.cells.item(2).innerText) //giftcert name
    document.getElementById("description").setAttribute("placeholder", row.cells.item(3).innerText) //description
    document.getElementById("price").setAttribute("placeholder", row.cells.item(4).innerText) //price
    document.getElementById("duration").setAttribute("placeholder", row.cells.item(5).innerText) //duration
}


function showAlert(message){
    let placeholder = document.getElementById("alertPlaceholder")

    placeholder.innerHTML = [
        '<div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">',
          '<div class="toast-header">',
            '<strong class="me-auto">Certifica 2</strong>',
            '<small>Notification</small>',
            '<button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>',
          '</div>',
          '<div class="toast-body">',
            `${message}`,
          '</div>',
        '</div>',
        ].join('')

    placeholder.hidden = false
    let alertToast = new bootstrap.Toast(document.getElementById('liveToast'))
    alertToast.show()
}
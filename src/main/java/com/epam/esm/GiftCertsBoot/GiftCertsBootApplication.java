package com.epam.esm.GiftCertsBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GiftCertsBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(GiftCertsBootApplication.class, args);
	}

}

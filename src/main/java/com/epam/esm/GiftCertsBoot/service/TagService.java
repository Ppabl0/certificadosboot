package com.epam.esm.GiftCertsBoot.service;

import com.epam.esm.GiftCertsBoot.Utils.Validator;
import com.epam.esm.GiftCertsBoot.model.Tag;
import com.epam.esm.GiftCertsBoot.repository.GiftCertRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService {
    private final GiftCertRepository repository;

    TagService(GiftCertRepository repository){
        this.repository = repository;
    }

    public Tag createTag(String tag){
            repository.insertTag(tag);
            return repository.getTagByName(tag);
    }

    public Tag findTagById(int id){
        return repository.getTagById(id);
    }

    public Tag findTagByName(String name){
        if (Validator.tagNameCheck(name)){
            return repository.getTagByName(name);
        } else {
            throw new IllegalArgumentException("Invalid tag name: " + name);
        }
    }

    public List<Tag> findAllTags(){
        return repository.getAllTags();
    }

    public int modifyTag(int id, String tagName){
        if (findTagByName(tagName) == null) {
            return repository.updateTag(id, tagName.toUpperCase());
        } else {
            throw new IllegalArgumentException("Tag with name: " + tagName + " already exists");
        }
    }

    public int deleteTag(int id){
        return repository.deleteTag(id);
    }
}

package com.epam.esm.GiftCertsBoot.service;

import com.epam.esm.GiftCertsBoot.model.Customer;
import com.epam.esm.GiftCertsBoot.model.DTO.GiftCertDTO;
import com.epam.esm.GiftCertsBoot.model.GiftCert;
import com.epam.esm.GiftCertsBoot.model.Tag;
import com.epam.esm.GiftCertsBoot.repository.GiftCertRepository;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class GiftCertService {
    private final GiftCertRepository giftCertRepository;

    public GiftCertService(GiftCertRepository giftCertRepository){
        this.giftCertRepository = giftCertRepository;
    }

    public boolean createGiftCert(GiftCertDTO giftCertDTO, String tags){
        GiftCert giftCert = addAndGetGiftCert(giftCertDTO);

        if (tags != null){
            addTagsToGiftCert(giftCert.getId(), tags);
        }

        return giftCert != null;
    }

    public GiftCertDTO findGiftCertById(int id){
        GiftCert giftCert = Optional.ofNullable(giftCertRepository.getGiftCertById(id))
                .orElseThrow(() -> new NoSuchElementException(String.format("Gift with id %d not found", id)));

        return buildDTO(giftCert);
    }

    public GiftCertDTO findGiftCertByName(String name){
        GiftCert giftCert = Optional.ofNullable(giftCertRepository.getGiftCertByName(name))
                .orElseThrow(() -> new NoSuchElementException(String.format("Gift with name: %s not found", name)));

        return buildDTO(giftCert);
    }

    public List<GiftCertDTO> findAllGiftCert(){
        List<GiftCert> giftCertList = giftCertRepository.getAllGiftCert();

        return buildDTOList(giftCertList);
    }

    public List<GiftCertDTO> findGiftCertByCustomerId(int customerId){
        Customer customer = Optional.of(giftCertRepository.getCustomerById(customerId))
                .orElseThrow( () ->
                        new NoSuchElementException(String.format("Customer with id: %d not found", customerId)));

        List<GiftCert> giftCertList = giftCertRepository.getGiftCertByCustomerId(customer.getId());

        return buildDTOList(giftCertList);
    }

    public List<GiftCertDTO> searchGiftCerts(String name, String description, String tag, String sortColumn, boolean desc){
        Integer tagId = ifTagExistsGetId(tag);
        sortColumn = sortColumn == null ? "" : sortColumn;

        List<GiftCert> giftCertList = giftCertRepository.searchGiftCertByNameOrDescriptionOrTag(name, description,
                                                                                               tagId, sortColumn, desc);

        return buildDTOList(giftCertList);
    }

    public int modifyGiftCert(int giftCertId, GiftCertDTO dto, String tags){
        boolean giftCertExist = giftCertRepository.getGiftCertById(giftCertId) != null;
        //verificar en endpoint que pasa si el body esta empty

        if (giftCertExist){
            Integer customerId = null;

            if (dto.getOwner() != null){
                Customer customer = giftCertRepository.getCustomerByName(dto.getOwner());
                if (customer != null){
                    customerId = customer.getId();
                } else {
                    throw new NoSuchElementException(String.format("customer with name: %s doesn't exist", dto.getOwner()));
                }
            }

            if (tags != null){
                addTagsToGiftCert(giftCertId, tags);
            }

            return giftCertRepository.updateGiftCert(giftCertId, dto.getGiftCertName(),
                    dto.getDescription(), dto.getPrice(), dto.getDuration(), customerId);

        } else {
            throw new NoSuchElementException(String.format("Gift with id %d not found", giftCertId));
        }

    }

    public void addTagsToGiftCert(int giftCertId, String tags){
        boolean giftCertExist = giftCertRepository.getGiftCertById(giftCertId) != null;

        if (giftCertExist) {
            String[] tagsArray = tags.split(";");
            addNewTagsToDB(tagsArray);
            linkTagsWithGiftCert(giftCertId, tagsArray);
        } else {
            throw new NoSuchElementException(String.format("Gift with id: %d not found", giftCertId));
        }
    }

    public int removeTagFromGiftCert(int giftCertId, String tagName){
        GiftCert giftCert = giftCertRepository.getGiftCertById(giftCertId);
        Tag tag = giftCertRepository.getTagByName(tagName);
        if (giftCert != null && tag != null) {
            return giftCertRepository.deleteTagFromGiftCert(giftCert.getId(), tag.getId());
        } else {
            return 0;
        }
    }

    public int deleteGiftCert(int id){
        return giftCertRepository.deleteGiftCert(id);
    }


    //HELPERS
    private void addNewTagsToDB(String[] tags){
        Arrays.stream(tags)
              .filter( tag -> giftCertRepository.getTagByName(tag) == null)
              .forEach(giftCertRepository::insertTag);
    }

    private GiftCert addAndGetGiftCert(GiftCertDTO dto){
        String owner = dto.getOwner();
        Customer customer = Optional.ofNullable(giftCertRepository.getCustomerByName(owner))
                .orElseThrow(() -> new NoSuchElementException(String.format("Customer with name %s not found", owner)));

        int customerId = customer.getId();
        String name = dto.getGiftCertName();
        String description = Optional.ofNullable(dto.getDescription()).orElse("");

        giftCertRepository.insertGiftCert(name, description, dto.getPrice(), dto.getDuration(), customerId);

        return giftCertRepository.getGiftCertByName(name);
    }

    private void linkTagsWithGiftCert(int giftCertId, String[] tags){
        Arrays.stream(tags)
                .map(giftCertRepository::getTagByName)
                .mapToInt(Tag::getId)
                .filter(tagId -> ! giftCertRepository.giftCertTagLinkExist(giftCertId, tagId))
                .forEach(tagId -> giftCertRepository.addTagToGiftCert(giftCertId, tagId));
    }

    private Integer ifTagExistsGetId(String tag){
        Tag existingTag = null;

        if ( tag != null && ! tag.isEmpty()){
            existingTag = giftCertRepository.getTagByName(tag);
        }

        if (existingTag != null) {
            return existingTag.getId();
        } else {
            return null;
        }
    }

    private GiftCertDTO buildDTO(GiftCert giftCert){
        String owner = giftCertRepository.getCustomerById(giftCert.getCustomerId()).getName();
        List<Tag> tags = giftCertRepository.getTagsByGiftCertId(giftCert.getId());

        return GiftCertDTO.of(giftCert, owner, tags);
    }

    private List<GiftCertDTO> buildDTOList(List<GiftCert> giftCertList){
        return giftCertList.stream().map(this::buildDTO).collect(Collectors.toList());
    }

}

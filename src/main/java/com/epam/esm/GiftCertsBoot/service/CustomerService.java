package com.epam.esm.GiftCertsBoot.service;

import com.epam.esm.GiftCertsBoot.Utils.Validator;
import com.epam.esm.GiftCertsBoot.model.Customer;
import com.epam.esm.GiftCertsBoot.model.DTO.CustomerDTO;
import com.epam.esm.GiftCertsBoot.repository.GiftCertRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class CustomerService {
    private final GiftCertRepository giftCertRepository;
    private final PasswordEncoder passwordEncoder;

    public CustomerService(GiftCertRepository giftCertRepository, PasswordEncoder passwordEncoder){
        this.giftCertRepository = giftCertRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public CustomerDTO findCustomerById(int id){
        Customer customer = Optional.ofNullable(giftCertRepository.getCustomerById(id))
                .orElseThrow(() -> new NoSuchElementException("Customer with id: " + id + " not found" ));

        return CustomerDTO.of(customer);
    }

    public CustomerDTO findCustomerByName(String name){
        if (Validator.customerNameCheck(name)){
            Customer customer = Optional.ofNullable(giftCertRepository.getCustomerByName(name))
                    .orElseThrow(() -> new NoSuchElementException("Customer with name: " + name + " not found"));

            return CustomerDTO.of(customer);
        } else {
            throw new IllegalArgumentException("Invalid customer name: " + name);
        }

    }

    public List<CustomerDTO> findAllCustomers(){
        List<Customer> customers = giftCertRepository.getAllCustomers();
        List<CustomerDTO> dtoList = new ArrayList<>();

        customers.forEach(customer -> dtoList.add(CustomerDTO.of(customer)));

        return dtoList;
    }

    public CustomerDTO createCustomer(CustomerDTO dto){
        if (hasValidData(dto)) {
            String name = dto.getName();

            giftCertRepository.insertCustomer(name, passwordEncoder.encode(dto.getPassword()));
            giftCertRepository.insertCustomerAuthority(name);

            return CustomerDTO.of(giftCertRepository.getCustomerByName(name));
        } else {
            throw new IllegalArgumentException("Invalid name or password, password must be at least 5 characters long");
        }
    }

    public int modifyCustomer(int id, CustomerDTO dto){
        if (hasValidData(dto)){
            String password = dto.getPassword();
            password =  password != null ? passwordEncoder.encode(password) : null;

            return giftCertRepository.updateCustomer(id, dto.getName(), password);
        } else {
            throw new IllegalArgumentException("Invalid name or password, password must be at least 5 characters long");
        }
    }

    public int deleteCustomer(int id){
        return giftCertRepository.deleteCustomer(id);
    }


    //HELPER
    private boolean hasValidData(CustomerDTO dto){
        boolean isValidData = false;
        String name = dto.getName();
        String password = dto.getPassword();

        if (giftCertRepository.getCustomerByName(name) != null){
            throw new IllegalArgumentException("customer with name: " + name + " already exists");
        }
        if (name != null){
            isValidData = Validator.customerNameCheck(name);
        }
        if (password != null){
            isValidData = Validator.passwordCheck(password);
        }

        return isValidData;
    }
}

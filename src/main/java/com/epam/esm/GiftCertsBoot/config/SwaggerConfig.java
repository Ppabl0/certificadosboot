package com.epam.esm.GiftCertsBoot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;


@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.epam.esm.GiftCertsBoot.controller"))
                .paths(PathSelectors.regex("/api/giftcerts.*|/api/tags.*"))
                .build()
                .apiInfo(info());
    }


    private ApiInfo info(){
        return new ApiInfo("Certificados de regalo",
                "REST API para ejercicio spring boot",
                "0.0.1",
                "uso local only",
                new springfox.documentation.service.Contact("Jose Pablo", "thpt/:/unaWebsait.oi","fake@mail.web"),
                "Licensia",
                "https/:/website.bus",
                Collections.emptyList());
    }
}

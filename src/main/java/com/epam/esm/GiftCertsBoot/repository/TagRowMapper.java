package com.epam.esm.GiftCertsBoot.repository;

import com.epam.esm.GiftCertsBoot.model.Tag;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TagRowMapper implements RowMapper<Tag> {
    @Override
    public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
        int id = rs.getInt(1);
        String tagName = rs.getString(2);

        return new Tag(id, tagName, null);
    }
}

package com.epam.esm.GiftCertsBoot.repository;

import com.epam.esm.GiftCertsBoot.model.Customer;
import com.epam.esm.GiftCertsBoot.model.GiftCert;
import com.epam.esm.GiftCertsBoot.model.Tag;

import java.math.BigDecimal;
import java.util.List;

public interface GiftCertRepository {

    GiftCert getGiftCertById(int id);
    GiftCert getGiftCertByName(String name);
    List<GiftCert> getAllGiftCert();
    List<GiftCert> getGiftCertByCustomerId(int customerId);
    int insertGiftCert(String name, String description, BigDecimal price, Integer duration, int customerId);
    List<GiftCert> searchGiftCertByNameOrDescriptionOrTag(String name, String description, Integer tag,
                                                          String sortColumn, boolean desc);
    int updateGiftCert(int id, String giftCertName, String description, BigDecimal price,
                       Integer duration, Integer customerId);
    int deleteGiftCert(int id);

    int insertTag(String name);
    Tag getTagById(int id);
    Tag getTagByName(String tagName);
    List<Tag> getTagsByGiftCertId(int id);
    List<Tag> getAllTags();
    int updateTag(int id, String name);
    int deleteTag(int id);
    boolean giftCertTagLinkExist(int giftCertId, int tagId);

    void addTagToGiftCert(int giftCertId, int tagId);
    int deleteTagFromGiftCert(int giftCertId, int tagId);

    Customer getCustomerById(int id);
    Customer getCustomerByName(String name);
    List<Customer> getAllCustomers();
    int insertCustomer(String name, String password);
    int updateCustomer(int id, String name, String password);
    int deleteCustomer(int id);

    int insertCustomerAuthority(String name);

}

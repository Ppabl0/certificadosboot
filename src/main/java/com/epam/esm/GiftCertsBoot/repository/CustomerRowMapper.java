package com.epam.esm.GiftCertsBoot.repository;

import com.epam.esm.GiftCertsBoot.model.Customer;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CustomerRowMapper implements RowMapper<Customer> {

    @Override
    public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Customer customer = new Customer();
        customer.setId(rs.getInt(1));
        customer.setName(rs.getString(2));
        customer.setPassword(rs.getString(3));
        return customer;
    }
}

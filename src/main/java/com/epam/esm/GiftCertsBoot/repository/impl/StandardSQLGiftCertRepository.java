package com.epam.esm.GiftCertsBoot.repository.impl;

import com.epam.esm.GiftCertsBoot.model.Customer;
import com.epam.esm.GiftCertsBoot.model.GiftCert;
import com.epam.esm.GiftCertsBoot.model.Tag;
import com.epam.esm.GiftCertsBoot.repository.CustomerRowMapper;
import com.epam.esm.GiftCertsBoot.repository.GiftCertRepository;
import com.epam.esm.GiftCertsBoot.repository.GiftCertRowMapper;
import com.epam.esm.GiftCertsBoot.repository.TagRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

@Repository
public class StandardSQLGiftCertRepository implements GiftCertRepository {
    private final JdbcTemplate jdbcTemplate;
    private final GiftCertRowMapper giftCertRowMapper;
    private final TagRowMapper tagRowMapper;
    private final CustomerRowMapper customerRowMapper;

    public StandardSQLGiftCertRepository(DataSource dataSource, GiftCertRowMapper giftCertRowMapper,
                                         TagRowMapper tagRowMapper, CustomerRowMapper customerRowMapper){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.giftCertRowMapper = giftCertRowMapper;
        this.tagRowMapper = tagRowMapper;
        this.customerRowMapper = customerRowMapper;
    }


    @Override
    public int insertGiftCert(String name, String description, BigDecimal price, Integer duration, int customerId){
        String sqlQuery =
                "INSERT INTO GIFTCERTS (GIFTNAME, DESCRIPTION, PRICE, DURATION, CUSTOMER_ID) VALUES (?, ?, ?, ?, ?);";

        return jdbcTemplate.update(sqlQuery, name, description, price, duration, customerId);
    }

    @Override
    public GiftCert getGiftCertById(int giftCertId){
        String sqlQuery = "SELECT * FROM giftcerts WHERE id = ?;";

        List<GiftCert> result = jdbcTemplate.query(sqlQuery, giftCertRowMapper, giftCertId);

        if (result.size() == 0){
            return null;
        } else {
            return result.get(0);
        }
    }

    @Override
    public GiftCert getGiftCertByName(String name) {
        String sqlQuery = "SELECT * FROM giftcerts WHERE giftname = ?;";

        List<GiftCert> result = jdbcTemplate.query(sqlQuery, giftCertRowMapper, name);

        if (result.size() == 0){
            return null;
        } else {
            return result.get(0);
        }
    }

    @Override
    public List<GiftCert> getGiftCertByCustomerId(int customerId){
        String sqlQuery = "SELECT * FROM giftcerts JOIN customers ON customer_id = customers.id WHERE customer_id = ?;";

        return jdbcTemplate.query(sqlQuery, giftCertRowMapper, customerId);
    }

    @Override
    public List<GiftCert> searchGiftCertByNameOrDescriptionOrTag(String name, String description, Integer tagId,
                                                                 String sortColumn, boolean isDesc){
        StringBuilder sqlQuery;
        String sortOrder = isDesc ? "DESC" : "";

        if (tagId == null){
            sqlQuery = new StringBuilder("SELECT * FROM giftcerts WHERE ");
            addNameAndOrDescriptionToQuery(sqlQuery, name, description);
        } else {
            sqlQuery = new StringBuilder("SELECT * FROM giftcerts JOIN giftcert_tag ON id=gift_id WHERE tag_id=").append(tagId);

            if (name != null || description != null) {
                sqlQuery.append(" AND ");
                addNameAndOrDescriptionToQuery(sqlQuery, name, description);
            }
        }

        addSortingColumnAndOrder(sqlQuery, sortColumn, sortOrder);


        return jdbcTemplate.query(sqlQuery.toString(), giftCertRowMapper);
    }


    @Override
    public int updateGiftCert(int id, String giftCertName, String description, BigDecimal price,
                              Integer duration, Integer customerId){
        StringBuilder sqlQuery = new StringBuilder("UPDATE giftcerts SET ");

        if (giftCertName != null){
            sqlQuery.append("giftname = '").append(giftCertName).append("', ");
        }
        if (description != null){
            sqlQuery.append("description = '").append(description).append("', ");
        }
        if (price != null){
            sqlQuery.append("price = ").append(price).append(", ");
        }
        if (duration != null){
            sqlQuery.append("duration = ").append(duration).append(", ");
        }
        if (customerId != null){
            sqlQuery.append("customer_id = ").append(customerId).append(", ");
        }

        sqlQuery.deleteCharAt(sqlQuery.lastIndexOf(","));

        sqlQuery.append("WHERE id = ?;");

        return jdbcTemplate.update(sqlQuery.toString(), id);
    }

    @Override
    public int deleteGiftCert(int id){
        String sqlQuery = "DELETE FROM giftcerts WHERE id = ?";
        return jdbcTemplate.update(sqlQuery, id);
    }


    public List<GiftCert> getAllGiftCert(){
        String sqlQuery = "SELECT * FROM GIFTCERTS";

        return jdbcTemplate.query(sqlQuery, giftCertRowMapper);
    }

    @Override
    public int insertTag(String tagName) {
        String sqlQuery = "INSERT INTO tags (name) VALUES (?);";
        return jdbcTemplate.update(sqlQuery, tagName.toUpperCase(Locale.ROOT));
    }

    @Override
    public Tag getTagById(int id){
        String sqlQuery = "SELECT * FROM tags WHERE id = ?";
        List<Tag> tags = jdbcTemplate.query(sqlQuery, tagRowMapper, id);
        return tags.size() == 0 ? null : tags.get(0);
    }

    @Override
    public Tag getTagByName(String tagName){
        String sqlQuery = "SELECT * FROM tags WHERE name = UPPER(?);";

        List<Tag> tags = jdbcTemplate.query(sqlQuery, tagRowMapper, tagName);
        return tags.size() == 0 ? null : tags.get(0);
    }

    @Override
    public List<Tag> getAllTags(){
        String sqlQuery = "SELECT * FROM tags;";
        return jdbcTemplate.query(sqlQuery, tagRowMapper);
    }

    @Override
    public int updateTag(int id, String name){
        String sqlQuery = "UPDATE tags SET name = ? WHERE id = ?";
        return jdbcTemplate.update(sqlQuery, name, id);
    }

    @Override
    public int deleteTag(int id){
        String sqlQuery = "DELETE FROM tags WHERE id = ?";
        return jdbcTemplate.update(sqlQuery, id);
    }


    @Override
    public List<Tag> getTagsByGiftCertId(int giftCertId){
        String sqlQuery = "SELECT tags.id, name FROM tags JOIN giftcert_tag ON tag_id = tags.id WHERE gift_id = ?";
        return jdbcTemplate.query(sqlQuery, tagRowMapper, giftCertId);
    }

    @Override
    public void addTagToGiftCert(int giftCertId, int tagId) {
        String sqlQuery = "INSERT INTO giftcert_tag (gift_id, tag_id) VALUES (?, ?);";
        jdbcTemplate.update(sqlQuery, giftCertId, tagId);
    }

    @Override
    public boolean giftCertTagLinkExist(int giftCertId, int tagId){
        String query = "SELECT COUNT(1) FROM giftcert_tag WHERE gift_id = ? AND tag_id = ?;";
        Integer x = jdbcTemplate.queryForObject(query, Integer.class, giftCertId, tagId);

        return x != null && x > 0;
    }

    @Override
    public int deleteTagFromGiftCert(int giftCertId, int tagId){
        String sqlQuery = "DELETE FROM giftcert_tag WHERE gift_id = ? AND tag_id = ?;";
        return jdbcTemplate.update(sqlQuery, giftCertId, tagId);
    }

    @Override
    public Customer getCustomerById(int id){
        String sqlQuery = "SELECT * FROM customers WHERE id = ?;";
        List<Customer> customers = jdbcTemplate.query(sqlQuery, customerRowMapper, id);

        if (customers.size() == 0){
            return null;
        } else {
            return customers.get(0);
        }
    }

    @Override
    public Customer getCustomerByName(String name){
        String sqlQuery = "SELECT * FROM customers WHERE name = ?;";
        List<Customer> customer = jdbcTemplate.query(sqlQuery, customerRowMapper, name);

        if (customer.size() == 0){
            return null;
        } else {
            return customer.get(0);
        }
    }

    @Override
    public List<Customer> getAllCustomers(){
        String sqlQuery = "SELECT * FROM customers;";
        return jdbcTemplate.query(sqlQuery, customerRowMapper);
    }

    @Override
    public int insertCustomer(String name, String password){
        String sqlQuery = "INSERT INTO customers (name, password) VALUES (?, ?)";
        return jdbcTemplate.update(sqlQuery, name, password);
    }

    @Override
    public int insertCustomerAuthority(String name){
        String sqlQuery = "INSERT INTO authorities (username, authority) VALUES (?, ?)";
        return jdbcTemplate.update(sqlQuery, name, "ROLE_CLIENTE");
    }

    @Override
    public int updateCustomer(int id, String name, String password){
        StringBuilder sqlQuery = new StringBuilder("UPDATE customers SET ");

        if (name != null){
            sqlQuery.append("name = '").append(name).append("', ");
        }
        if (password != null){
            sqlQuery.append("password = '").append(password).append("', ");
        }

        sqlQuery.deleteCharAt(sqlQuery.lastIndexOf(","));

        sqlQuery.append("WHERE id = ?;");

        return jdbcTemplate.update(sqlQuery.toString(), id);
    }

    @Override
    public int deleteCustomer(int id){
        String sqlQuery = "DELETE FROM customers WHERE id = ?;";
        return jdbcTemplate.update(sqlQuery, id);
    }



    //HELPER METHODS
    private void addNameAndOrDescriptionToQuery(StringBuilder query, String name, String description){
        if (name != null && description != null){
            query.append("giftname ILIKE '%").append(name)
                    .append("%' AND description ILIKE '%").append(description)
                    .append("%' ");
        } else if (description == null) {
            query.append("giftname ILIKE '%").append(name).append("%' ");
        } else {
            query.append("description ILIKE '%").append(description).append("%' ");
        }
    }

    private void addSortingColumnAndOrder(StringBuilder query, String sortColumn, String sortOrder){
        if (sortColumn.equalsIgnoreCase("name")){
            query.append("ORDER BY giftname ").append(sortOrder).append(", create_date");
        } else if (sortColumn.equalsIgnoreCase("date")){
            query.append("ORDER BY create_date ").append(sortOrder).append(", giftname");
        }
    }
}

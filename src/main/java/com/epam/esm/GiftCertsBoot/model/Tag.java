package com.epam.esm.GiftCertsBoot.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Tag {
    private Integer id;
    private String tagName;
    private String errorMessage;
}

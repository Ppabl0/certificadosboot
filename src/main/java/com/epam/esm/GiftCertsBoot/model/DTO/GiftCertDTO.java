package com.epam.esm.GiftCertsBoot.model.DTO;

import com.epam.esm.GiftCertsBoot.model.GiftCert;
import com.epam.esm.GiftCertsBoot.model.Tag;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GiftCertDTO {
    private int id;
    private String owner;
    private String giftCertName;
    private String description;
    private BigDecimal price;
    private Integer duration;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss[.SSSSSS]")
    private LocalDateTime createDate;
    @JsonFormat(pattern = "yyy-MM-dd HH:mm:ss[.SSSSSS]")
    private LocalDateTime lastUpdateDate;
    private List<Tag> tags;
    private String errorMessage;

    public boolean hasValidInsertData(){
        return giftCertName != null && price != null && duration != null && owner != null;
    }

    public boolean giftCertFieldsAreValid(){
        return ! (giftCertName == null && description == null && price == null && duration == null && owner == null);
    }

    public static GiftCertDTO of(GiftCert giftCert){
        GiftCertDTO giftCertDTO = new GiftCertDTO();

        giftCertDTO.setId(giftCert.getId());
        giftCertDTO.setGiftCertName(giftCert.getGiftCertName());
        giftCertDTO.setDescription(giftCert.getDescription());
        giftCertDTO.setPrice(giftCert.getPrice());
        giftCertDTO.setDuration(giftCert.getDuration());
        giftCertDTO.setCreateDate(giftCert.getCreateDate());
        giftCertDTO.setLastUpdateDate(giftCert.getLastUpdateDate());

        return giftCertDTO;
    }

    public static GiftCertDTO of(GiftCert giftCert, String owner, List<Tag> tags){
        GiftCertDTO giftCertDTO = new GiftCertDTO();

        giftCertDTO.setId(giftCert.getId());
        giftCertDTO.setOwner(owner);
        giftCertDTO.setGiftCertName(giftCert.getGiftCertName());
        giftCertDTO.setDescription(giftCert.getDescription());
        giftCertDTO.setPrice(giftCert.getPrice());
        giftCertDTO.setDuration(giftCert.getDuration());
        giftCertDTO.setCreateDate(giftCert.getCreateDate());
        giftCertDTO.setLastUpdateDate(giftCert.getLastUpdateDate());
        giftCertDTO.setTags(tags);

        return giftCertDTO;
    }
}

package com.epam.esm.GiftCertsBoot.controller.api;

import com.epam.esm.GiftCertsBoot.model.Tag;
import com.epam.esm.GiftCertsBoot.service.TagService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/tags")
public class TagController {
    private final TagService tagService;

    TagController(TagService tagService){
        this.tagService = tagService;
    }

    @PostMapping
    public ResponseEntity<Tag> create(@RequestParam String tag, UriComponentsBuilder uriComponentsBuilder){
        if (tagService.findTagByName(tag) == null){
            Tag result = tagService.createTag(tag);

            URI address = uriComponentsBuilder.path("/tags/")
                    .path(String.valueOf(result.getId()))
                    .build()
                    .toUri();

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(address);

            return ResponseEntity.status(HttpStatus.CREATED).headers(headers).body(result);

        } else {
            Tag response = new Tag();
            response.setErrorMessage("Tag with name: " + tag + " already exists");

            return ResponseEntity.status(HttpStatus.CONFLICT).body(response);

        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Tag> findbyId(@PathVariable int id){
        Tag result = tagService.findTagById(id);

        if (result != null){
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            throw new NoSuchElementException("Tag with id: " + id + " not found");
        }
    }

    @GetMapping("/name")
    public ResponseEntity<Tag> findByName(@RequestParam String name){
        Tag result = tagService.findTagByName(name);

        if (result != null){
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            throw new NoSuchElementException("Tag with name: " + name + "not found");
        }
    }

    @GetMapping
    public ResponseEntity<List<Tag>> findAll(){
        return ResponseEntity.status(HttpStatus.OK).body(tagService.findAllTags());
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Tag> modify(@PathVariable int id, @RequestParam String tag){
        if (tagService.modifyTag(id, tag) > 0){
             Tag result = tagService.findTagById(id);
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            throw new NoSuchElementException("Tag with id: " + id + " not found");
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable int id){
        if (tagService.deleteTag(id) > 0) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Tag with given id not found, no tag was deleted");
        }
    }
}

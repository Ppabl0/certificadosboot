package com.epam.esm.GiftCertsBoot.controller.api;

import com.epam.esm.GiftCertsBoot.model.DTO.*;
import com.epam.esm.GiftCertsBoot.service.GiftCertService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/giftcerts")
public class GiftCertController {
    private final GiftCertService giftCertService;

    GiftCertController(GiftCertService giftCertService){
        this.giftCertService = giftCertService;
    }


    @PostMapping
    public ResponseEntity<GiftCertDTO> create(@RequestBody GiftCertDTO giftCertDTO,
                                                   @RequestParam(required = false) String tags,
                                                   UriComponentsBuilder uriComponentsBuilder){

        if (giftCertDTO.hasValidInsertData()){
            giftCertService.createGiftCert(giftCertDTO, tags);
            GiftCertDTO result = giftCertService.findGiftCertByName(giftCertDTO.getGiftCertName());

            URI address = uriComponentsBuilder.path("/api/giftcerts/")
                    .path(String.valueOf(result.getId()))
                    .build()
                    .toUri();

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(address);

            return ResponseEntity.status(HttpStatus.CREATED)
                    .headers(headers)
                    .body(result);
        } else {
            throw new IllegalArgumentException("Invalid data, name, price, description, and owner shouldn't be empty");
        }


    }

    @GetMapping("/{id}")
    public ResponseEntity<GiftCertDTO> findById(@PathVariable int id){
        GiftCertDTO result = giftCertService.findGiftCertById(id);

        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @GetMapping
    public ResponseEntity<List<GiftCertDTO>> findAll(){
        return ResponseEntity.status(HttpStatus.OK)
                .body(giftCertService.findAllGiftCert());
    }

    @GetMapping("/customer/{customerId}")
    public ResponseEntity<List<GiftCertDTO>> findByCustomerId(@PathVariable int customerId){
        List<GiftCertDTO> result = giftCertService.findGiftCertByCustomerId(customerId);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @GetMapping("/search")
    public ResponseEntity<List<GiftCertDTO>> search(@RequestParam(required = false) String name,
                                                 @RequestParam(required = false) String description,
                                                 @RequestParam(required = false) String tag,
                                                 @RequestParam(required = false) String sortColumn,
                                                 @RequestParam(required = false) boolean desc){

        if (name == null && description == null && tag == null) {
            throw new IllegalArgumentException("All search fields are empty");
        }

        return ResponseEntity.status(HttpStatus.OK)
                    .body(giftCertService.searchGiftCerts(name, description, tag, sortColumn, desc));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<GiftCertDTO> modify(@PathVariable int id,
                                              @RequestBody GiftCertDTO newData,
                                              @RequestParam(required = false) String tags){

        if (newData.giftCertFieldsAreValid()){
            giftCertService.modifyGiftCert(id, newData, tags);
            return ResponseEntity.status(HttpStatus.OK).body(giftCertService.findGiftCertById(id));
        } else {
            throw new IllegalArgumentException("Invalid input data");
        }
    }

    @PatchMapping("/{id}/tags")
    public ResponseEntity<GiftCertDTO> addTags(@PathVariable int id, @RequestParam String tags){
        if (! tags.isEmpty()) {
            giftCertService.addTagsToGiftCert(id, tags);
            return ResponseEntity.status(HttpStatus.OK).body(giftCertService.findGiftCertById(id));
        } else {
            throw new IllegalArgumentException("Invalid tags, tag should have a maximum of 64 letters" +
                    "and multiple tags must be separated by ';'");
        }
    }

    @DeleteMapping("/{id}/tags")
    public ResponseEntity<String> deleteTag(@PathVariable int id, @RequestParam String tag){
            int result = giftCertService.removeTagFromGiftCert(id, tag);

            if (result > 0) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body("gifCert with given id or tag with given name not found");
            }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable int id){
        int result = giftCertService.deleteGiftCert(id);

        if (result > 0){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
        } else {
            throw new NoSuchElementException(String.format("Resource with id: %d not found", id));

        }
    }

}

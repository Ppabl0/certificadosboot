package com.epam.esm.GiftCertsBoot.controller;

import com.epam.esm.GiftCertsBoot.model.DTO.CustomerDTO;
import com.epam.esm.GiftCertsBoot.model.DTO.GiftCertDTO;
import com.epam.esm.GiftCertsBoot.model.Tag;
import com.epam.esm.GiftCertsBoot.service.CustomerService;
import com.epam.esm.GiftCertsBoot.service.GiftCertService;
import com.epam.esm.GiftCertsBoot.service.TagService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class GiftCertViewController {
    private final CustomerService customerService;
    private final GiftCertService giftCertService;
    private final TagService tagService;

    public GiftCertViewController(CustomerService customerService, GiftCertService giftCertService,
                                  TagService tagService){
        this.customerService = customerService;
        this.giftCertService = giftCertService;
        this.tagService = tagService;
    }

    @GetMapping("/home")
    public String index(Model model, Authentication auth){
        boolean isAdmin = false;
        if (auth!= null){
            isAdmin = auth.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .anyMatch(authority -> authority.equals("ROLE_ADMIN"));
        }

        model.addAttribute("auth", auth);
        model.addAttribute("isAdmin", isAdmin);

        return "index";
    }

    @GetMapping("/user")
    public String giftCustomer(Model model, Authentication authentication){
        CustomerDTO customer = customerService.findCustomerByName(authentication.getName());
        List<GiftCertDTO> giftCertList = giftCertService.findGiftCertByCustomerId(customer.getId());

        model.addAttribute("certs", giftCertList);
        model.addAttribute("auth", authentication);
        return "user";
    }

    @GetMapping("/gifts")
    public String all(Model model){
        List<GiftCertDTO> certsa = giftCertService.findAllGiftCert();
        model.addAttribute("certs", certsa);
        return "giftcerts";
    }

    @GetMapping("/tags")
    public String allTags(Model model){
        List<Tag> tags = tagService.findAllTags();
        model.addAttribute("tags", tags);
        return "tags";
    }

    @GetMapping("/users")
    public String allUsers(Model model){
        List<CustomerDTO> customers = customerService.findAllCustomers();
        model.addAttribute("users", customers);
        return "users";
    }

    @GetMapping("/duo")
    public String duo(){
        return "duo";
    }


}

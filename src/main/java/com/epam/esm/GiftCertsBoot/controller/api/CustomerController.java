package com.epam.esm.GiftCertsBoot.controller.api;

import com.epam.esm.GiftCertsBoot.model.DTO.CustomerDTO;
import com.epam.esm.GiftCertsBoot.service.CustomerService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {
    private final CustomerService customerService;

    CustomerController(CustomerService customerService){
        this.customerService = customerService;
    }


    @GetMapping("/{id}")
    public ResponseEntity<CustomerDTO> findById(@PathVariable int id){
        CustomerDTO dto = customerService.findCustomerById(id);

        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @GetMapping("/name")
    public ResponseEntity<CustomerDTO> findByName(@RequestParam String name){
        CustomerDTO dto = customerService.findCustomerByName(name);

        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @GetMapping
    public ResponseEntity<List<CustomerDTO>> findAll(){
        List<CustomerDTO> result = customerService.findAllCustomers();

        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PostMapping
    public ResponseEntity<CustomerDTO> create(@RequestBody CustomerDTO dto,
                                              UriComponentsBuilder uriComponentsBuilder){
         if (dto.getName() != null && dto.getPassword() != null){
             CustomerDTO result = customerService.createCustomer(dto);

             URI address = uriComponentsBuilder.path("/api/customers/")
                     .path(String.valueOf(result.getId()))
                     .build()
                     .toUri();

             HttpHeaders headers = new HttpHeaders();
             headers.setLocation(address);

             return ResponseEntity.status(HttpStatus.CREATED)
                     .headers(headers)
                     .body(result);
            } else {
                throw new IllegalArgumentException("Missing required field(s)");
            }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<CustomerDTO> modify(@PathVariable int id, @RequestBody CustomerDTO dto){

        boolean customerExist = customerService.findCustomerById(id) != null;

        if (customerExist && ! (dto.getName() == null && dto.getPassword() == null)){
            customerService.modifyCustomer(id, dto);

            return ResponseEntity.status(HttpStatus.OK).body(customerService.findCustomerById(id));
        } else {
            throw new NoSuchElementException(String.format("customer with id: %d not found", id));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable int id){
        int result = customerService.deleteCustomer(id);
        if (result > 0){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(String.format("customer with id: %d not found", id));
        }
    }

}

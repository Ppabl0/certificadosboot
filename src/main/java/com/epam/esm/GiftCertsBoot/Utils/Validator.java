package com.epam.esm.GiftCertsBoot.Utils;

public class Validator {

    public static boolean tagNameCheck(String tagName){
        //At least 2 characters and only letters and underscore, check
        return tagName.matches("[a-zA-Z][a-zA-Z_]{1,63}");
    }

    public static boolean customerNameCheck(String customerName){
        //must start with letter, letters numbers and underscore allowed, check
        return customerName.matches("[a-zA-Z][a-zA-Z0-9_]{1,126}");
    }

    public static boolean passwordCheck(String password){
        //musta have at least 5 chars, letters numbers and some special chars allowed, check
        return password.matches("[a-zA-Z0-9_%$#]{5,}");
    }
}

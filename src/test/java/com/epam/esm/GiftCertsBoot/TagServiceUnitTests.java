package com.epam.esm.GiftCertsBoot;

import com.epam.esm.GiftCertsBoot.model.Tag;
import com.epam.esm.GiftCertsBoot.repository.GiftCertRepository;
import com.epam.esm.GiftCertsBoot.service.TagService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class TagServiceUnitTests {
    @Mock
    private GiftCertRepository repository;
    @InjectMocks
    private TagService tagService;


    @Test
    @DisplayName("Test creating a new valid tag")
    void createTagTest(){
        Tag expected = new Tag(72, "FURNITURE", null);
        when(repository.getTagByName("FURNITURE")).thenReturn(expected);

        assertEquals(expected, tagService.createTag("FURNITURE"));

    }

    @Test
    @DisplayName("Test find tag with valid id")
    void findTagByIdTest(){
        Tag expected = new Tag(72, "FURNITURE", null);
        when(repository.getTagById(72)).thenReturn(expected);

        assertEquals(expected, tagService.findTagById(72));
    }

    @Test
    @DisplayName("Test find tag with valid name")
    void findTagByNameTest(){
        Tag expected = new Tag(72, "FURNITURE", null);
        when(repository.getTagByName("FURNITURE")).thenReturn(expected);

        assertEquals(expected, tagService.findTagByName("FURNITURE"));
    }

    @ParameterizedTest
    @DisplayName("Test find tag with invalid name should throw exception")
    @ValueSource(strings = {"", "()", "_fruta", "{}", "?&%$!\"`~*'.,;:-"})
    void findInvalidTagNameShouldThrow(String input){
        assertThrows(IllegalArgumentException.class, () -> tagService.findTagByName(input));
    }

    @Test
    @DisplayName("Test find all tags")
    void findAllTagsTest(){
        Tag tag1 = new Tag(72, "FURNITURE", null);
        Tag tag2 = new Tag(73, "Appliance", null);
        List<Tag> expected = new ArrayList<>();
        expected.add(tag1);
        expected.add(tag2);

        when(repository.getAllTags()).thenReturn(expected);

        assertEquals(expected, tagService.findAllTags());
    }

    @Test
    @DisplayName("Test modify tag name")
    void modifyTagTest(){
        when(repository.getTagByName(any())).thenReturn(null);
        when(repository.updateTag(1, "SHOWER")).thenReturn(1);

        assertTrue(tagService.modifyTag(1, "SHOWER") > 0);
    }

    @Test
    @DisplayName("Test delete existing tag")
    void deleteTagTest(){
        when(repository.deleteTag(72)).thenReturn(1);

        assertTrue(repository.deleteTag(72) > 0);
    }

}

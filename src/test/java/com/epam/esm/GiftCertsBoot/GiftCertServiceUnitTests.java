package com.epam.esm.GiftCertsBoot;

import com.epam.esm.GiftCertsBoot.model.Customer;
import com.epam.esm.GiftCertsBoot.model.DTO.GiftCertDTO;
import com.epam.esm.GiftCertsBoot.model.GiftCert;
import com.epam.esm.GiftCertsBoot.model.Tag;
import com.epam.esm.GiftCertsBoot.repository.GiftCertRepository;
import com.epam.esm.GiftCertsBoot.service.GiftCertService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GiftCertServiceUnitTests {
    private GiftCertRepository giftCertsRepository;
    private GiftCertService giftCertService;

    @BeforeEach
    void setupService() {
        giftCertsRepository = mock(GiftCertRepository.class);
        giftCertService = new GiftCertService(giftCertsRepository);
    }

    @Test
    @DisplayName("Test create giftCert without tags")
    void createGiftCertNoTagsTest() {
        GiftCertDTO input = new GiftCertDTO(1, "Mango", "Certificate", null, null, null, null, null, null, null);
        GiftCert expectedGiftCert = new GiftCert(1, "Certificate", null, null, null, null, null, null);
        Customer expectedOwner = new Customer(1, "Mango", null);

        when(giftCertsRepository.getCustomerByName("Mango")).thenReturn(expectedOwner);
        when(giftCertsRepository.getGiftCertByName("Certificate")).thenReturn(expectedGiftCert);

        assertTrue(giftCertService.createGiftCert(input, null));
    }

    @Test
    @DisplayName("Test create giftCert with non existing customer should throw exception")
    void createGiftCertWithNonExistingCustomer() {
        GiftCertDTO input = new GiftCertDTO(1, "Mango", "Certificate", null, null, null, null, null, null, null);

        when(giftCertsRepository.getCustomerByName(anyString())).thenReturn(null);

        assertThrows(NoSuchElementException.class, () -> giftCertService.createGiftCert(input, null));
        assertThrows(NoSuchElementException.class, () -> giftCertService.createGiftCert(input, "FRUITS"));
    }

    @Test
    @DisplayName("Test create giftCert with 1 or more tags")
    void createGiftCertWithTagsTest() {
        GiftCertDTO input = new GiftCertDTO(1, "Mango", "Certificate", null, null, null, null, null, null, null);
        GiftCert expectedGiftCert = new GiftCert(1, "Certificate", null, null, null, null, null, null);
        Customer expectedOwner = new Customer(1, "Mango", null);

        when(giftCertsRepository.getCustomerByName("Mango")).thenReturn(expectedOwner);
        when(giftCertsRepository.getGiftCertByName("Certificate")).thenReturn(expectedGiftCert);
        when(giftCertsRepository.getGiftCertById(anyInt())).thenReturn(new GiftCert());
        when(giftCertsRepository.getTagByName(anyString())).thenReturn(new Tag(1, "FRUIT", null));

        assertTrue(giftCertService.createGiftCert(input, "FRUIT"));
        assertTrue(giftCertService.createGiftCert(input, "FRUIT, CLOTHES"));
    }

    @Test
    @DisplayName("Test find giftCert by id with valid id")
    void findGiftCertByIdValidId() {
        GiftCert gift = new GiftCert(1, "Clothing", "valid for clothing", BigDecimal.valueOf(1000), 30,
                LocalDateTime.now(), LocalDateTime.now(), 1);
        Customer customer = new Customer(1, "Mango", "x");

        when(giftCertsRepository.getGiftCertById(1)).thenReturn(gift);
        when(giftCertsRepository.getTagsByGiftCertId(anyInt())).thenReturn(null);
        when(giftCertsRepository.getCustomerById(anyInt())).thenReturn(customer);

        GiftCertDTO expected = GiftCertDTO.of(gift);
        expected.setOwner("Mango");

        GiftCertDTO result = giftCertService.findGiftCertById(1);
        assertEquals(result, expected);
    }

    @Test
    @DisplayName("Test find giftCert by id with invalid id")
    void findGiftCertByIdInvalidId() {
        when(giftCertsRepository.getGiftCertById(anyInt())).thenReturn(null);

        assertThrows(NoSuchElementException.class, () -> giftCertService.findGiftCertById(0));
    }

    @Test
    @DisplayName("Test find giftCert by name with existing name")
    void findGiftCertByName() {
        GiftCert gift = new GiftCert(1, "Clothing", "valid for clothing", BigDecimal.valueOf(1000), 30,
                LocalDateTime.now(), LocalDateTime.now(), 1);
        Customer customer = new Customer(1, "Mango", "x");

        when(giftCertsRepository.getGiftCertByName("Clothing")).thenReturn(gift);
        when(giftCertsRepository.getTagsByGiftCertId(anyInt())).thenReturn(null);
        when(giftCertsRepository.getCustomerById(anyInt())).thenReturn(customer);

        GiftCertDTO expected = GiftCertDTO.of(gift);
        expected.setOwner("Mango");

        GiftCertDTO result = giftCertService.findGiftCertByName("Clothing");
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Test find giftCert by name with non-existing name")
    void findGiftCertByNonExistingName() {
        when(giftCertsRepository.getGiftCertByName(anyString())).thenReturn(null);

        assertThrows(NoSuchElementException.class, () -> giftCertService.findGiftCertByName("Gift certificate name"));
    }

    @Test
    @DisplayName("Test findAll giftCerts")
    void findAllGiftCerts() {
        GiftCert giftCert1 = new GiftCert(1, "Clothing Gift Card", "valid for clothing", BigDecimal.valueOf(1000), 30,
                LocalDateTime.now(), LocalDateTime.now(), 1);
        GiftCert giftCert2 = new GiftCert(2, "Furniture Gift Card", "valid for furniture", BigDecimal.valueOf(1000), 30,
                LocalDateTime.now(), LocalDateTime.now(), 1);
        Customer customer = new Customer(1, "Mango", "x");
        List<GiftCert> data = new ArrayList<>();
        data.add(giftCert1);
        data.add(giftCert2);

        when(giftCertsRepository.getAllGiftCert()).thenReturn(data);
        when(giftCertsRepository.getCustomerById(anyInt())).thenReturn(customer);
        when(giftCertsRepository.getTagsByGiftCertId(anyInt())).thenReturn(null);

        GiftCertDTO dto1 = GiftCertDTO.of(giftCert1);
        GiftCertDTO dto2 = GiftCertDTO.of(giftCert2);
        dto1.setOwner("Mango");
        dto2.setOwner("Mango");
        List<GiftCertDTO> expected = new ArrayList<>();
        expected.add(dto1);
        expected.add(dto2);

        assertIterableEquals(expected, giftCertService.findAllGiftCert());
    }

    @Test
    @DisplayName("Test findGiftCerts by customer id")
    void findCustomerGiftCerts() {
        GiftCert giftCert1 = new GiftCert(1, "Clothing Gift Card", "valid for clothing", BigDecimal.valueOf(1000), 30,
                LocalDateTime.now(), LocalDateTime.now(), 1);
        GiftCert giftCert2 = new GiftCert(2, "Furniture Gift Card", "valid for furniture", BigDecimal.valueOf(1000), 30,
                LocalDateTime.now(), LocalDateTime.now(), 1);
        Customer customer = new Customer(1, "Mango", "x");
        List<GiftCert> data = new ArrayList<>();
        data.add(giftCert1);
        data.add(giftCert2);

        when(giftCertsRepository.getGiftCertByCustomerId(1)).thenReturn(data);
        when(giftCertsRepository.getCustomerById(anyInt())).thenReturn(customer);
        when(giftCertsRepository.getTagsByGiftCertId(anyInt())).thenReturn(null);

        GiftCertDTO dto1 = GiftCertDTO.of(giftCert1);
        GiftCertDTO dto2 = GiftCertDTO.of(giftCert2);
        dto1.setOwner("Mango");
        dto2.setOwner("Mango");
        List<GiftCertDTO> expected = new ArrayList<>();
        expected.add(dto1);
        expected.add(dto2);

        assertIterableEquals(expected, giftCertService.findGiftCertByCustomerId(1));
    }

    @Test
    @DisplayName("Test search returns non empty")
    void searchGiftCerts() {
        GiftCert giftCert1 = new GiftCert(1, "Clothing Gift Card", "valid for clothing",
                BigDecimal.valueOf(1000), 30, LocalDateTime.now(), LocalDateTime.now(), 1);
        GiftCert giftCert2 = new GiftCert(2, "Furniture Gift Card", "valid for furniture",
                BigDecimal.valueOf(1000), 30, LocalDateTime.now(), LocalDateTime.now(), 1);
        Customer customer = new Customer(1, "Mango", "x");
        List<GiftCert> data = new ArrayList<>();
        data.add(giftCert1);
        data.add(giftCert2);

        when(giftCertsRepository.searchGiftCertByNameOrDescriptionOrTag(anyString(), anyString(), anyInt(),
                anyString(), anyBoolean())).thenReturn(data);
        when(giftCertsRepository.getCustomerById(anyInt())).thenReturn(customer);
        when(giftCertsRepository.getTagByName(anyString())).thenReturn(new Tag(1, "FRUIT", null));
        when(giftCertsRepository.getTagsByGiftCertId(anyInt())).thenReturn(null);

        GiftCertDTO dto1 = GiftCertDTO.of(giftCert1);
        GiftCertDTO dto2 = GiftCertDTO.of(giftCert2);
        dto1.setOwner("Mango");
        dto2.setOwner("Mango");
        List<GiftCertDTO> expected = new ArrayList<>();
        expected.add(dto1);
        expected.add(dto2);

        assertIterableEquals(expected, giftCertService.searchGiftCerts("Coach gift card", "furniture gift",
                "FURNITURE", "asc", false));

    }

    @Test
    @DisplayName("Test search returns empty and not null")
    void searchEmpty() {
        when(giftCertsRepository.searchGiftCertByNameOrDescriptionOrTag(anyString(), anyString(), anyInt(),
                anyString(), anyBoolean())).thenReturn(new ArrayList<>());
        when(giftCertsRepository.getCustomerById(anyInt())).thenReturn(new Customer(1, "Mango", null));
        when(giftCertsRepository.getTagByName(anyString())).thenReturn(new Tag(1, "FRUIT", null));
        when(giftCertsRepository.getTagsByGiftCertId(anyInt())).thenReturn(null);

        List<GiftCertDTO> result = giftCertService.searchGiftCerts("Coach gift card", "furniture gift",
                "FURNITURE", "asc", false);

        assertNotNull(result);
        assertIterableEquals(new ArrayList<>(), result);
    }

    @Test
    @DisplayName("Test modify giftCert with valid input data")
    void modifyGiftcert() {
        when(giftCertsRepository.getGiftCertById(anyInt())).thenReturn(new GiftCert());
        when(giftCertsRepository.getCustomerByName(anyString())).thenReturn(new Customer(1, "Mango", null));
        when(giftCertsRepository.updateGiftCert(anyInt(), anyString(), anyString(), any(), anyInt(), anyInt()))
                .thenReturn(1);

        GiftCertDTO input = new GiftCertDTO(1, "Mango", "Gift card", "", BigDecimal.valueOf(1), 30,
                null, null, null, null);

        assertTrue(giftCertService.modifyGiftCert(1, input, null) > 0);
    }

    @Test
    @DisplayName("Test modify giftCert with non-existing id should throw exception")
    void modifyNonExistingGiftCert() {
        when(giftCertsRepository.getGiftCertById(anyInt())).thenReturn(null);

        assertThrows(NoSuchElementException.class, () -> giftCertService.modifyGiftCert(1, null, null));
    }

    @Test
    @DisplayName("Test if new owner doesn't exist should throw exception")
    void modifyOwnerToANonExistingCustomer() {
        GiftCertDTO input = new GiftCertDTO();
        input.setOwner("Dragon fruit");

        when(giftCertsRepository.getGiftCertById(anyInt())).thenReturn(new GiftCert());
        when(giftCertsRepository.getCustomerByName(anyString())).thenReturn(null);

        assertThrows(NoSuchElementException.class, () -> giftCertService.modifyGiftCert(1, input, null));
    }

    @Test
    @DisplayName("Test if giftCert doesn't exists should not add tags and throw exception")
    void addTagsToNonExistingGiftCert() {
        when(giftCertsRepository.getTagsByGiftCertId(anyInt())).thenReturn(null);

        assertThrows(NoSuchElementException.class, () -> giftCertService.addTagsToGiftCert(1, null));
    }

    @Test
    @DisplayName("Test remove tag from giftCert")
    void removeTag() {
        GiftCert giftCert = new GiftCert();
        giftCert.setId(1);
        Tag tag = new Tag();
        tag.setId(1);

        when(giftCertsRepository.getGiftCertById(anyInt())).thenReturn(giftCert);
        when(giftCertsRepository.getTagByName(anyString())).thenReturn(tag);
        when(giftCertsRepository.deleteTagFromGiftCert(anyInt(), anyInt())).thenReturn(1);

        assertTrue(giftCertService.removeTagFromGiftCert(1, "FRUITS") > 0);
    }

    @Test
    @DisplayName("Test remove tag from non existing giftCert")
    void removeTagFromNonExistingGiftCert() {
        when(giftCertsRepository.getGiftCertById(anyInt())).thenReturn(null);
        when(giftCertsRepository.getTagByName(anyString())).thenReturn(new Tag());

        assertEquals(0, giftCertService.removeTagFromGiftCert(5, "FRUITS"));
    }

    @Test
    @DisplayName("Test remove non existing tag from giftCert")
    void removeNonExistingTag() {
        when(giftCertsRepository.getGiftCertById(anyInt())).thenReturn(new GiftCert());
        when(giftCertsRepository.getTagByName(anyString())).thenReturn(null);

        assertEquals(0, giftCertService.removeTagFromGiftCert(5, "FRUITS"));
    }

    @Test
    @DisplayName("Test delete giftCert")
    void deleteGiftCert() {
        when(giftCertsRepository.deleteGiftCert(anyInt())).thenReturn(1);

        assertTrue(giftCertService.deleteGiftCert(2) > 0);
    }

    @Test
    @DisplayName("Test delete non existing giftCert")
    void deleteNonExistingGiftCert() {
        when(giftCertsRepository.deleteGiftCert(anyInt())).thenReturn(0);

        assertEquals(0, giftCertService.deleteGiftCert(1));
    }
}
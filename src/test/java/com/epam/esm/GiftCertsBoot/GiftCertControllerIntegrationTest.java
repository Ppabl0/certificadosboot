package com.epam.esm.GiftCertsBoot;

import com.epam.esm.GiftCertsBoot.controller.api.GiftCertController;
import com.epam.esm.GiftCertsBoot.model.DTO.GiftCertDTO;
import com.epam.esm.GiftCertsBoot.model.Tag;
import com.epam.esm.GiftCertsBoot.service.GiftCertService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(GiftCertController.class)
class GiftCertControllerIntegrationTest {
	@MockBean
	private GiftCertService giftCertService;
	@Autowired
	private MockMvc mockMvc;


	@WithMockUser
	@Test
	void createGiftCertTest() throws Exception {
		StringBuilder requestBody = new StringBuilder("{\"owner\":\"pineapple\",");
		requestBody.append("\"giftCertName\":\"GiftCard\",")
				.append("\"price\":100,")
				.append("\"duration\":30").append("}");

		GiftCertDTO giftCertDTO = new GiftCertDTO(6, "pineapple", "GiftCard", null, BigDecimal.valueOf(100),
				30, null, null, null, null);

		when(giftCertService.findGiftCertByName(any())).thenReturn(giftCertDTO);

		mockMvc.perform(post("/api/giftcerts").with(csrf().asHeader())
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestBody.toString()))
				.andExpect(status().isCreated())
				.andExpect(header().string(HttpHeaders.LOCATION, "http://localhost/api/giftcerts/6"))
				.andExpect(jsonPath("$.id").value(6))
				.andExpect(jsonPath("$.owner").value("pineapple"))
				.andExpect(jsonPath("$.giftCertName").value("GiftCard"))
				.andExpect(jsonPath("$.price").value(100.0))
				.andExpect(jsonPath("$.duration").value(30));
	}

	@Test
	@WithMockUser
	void findByIdTest() throws Exception {
		GiftCertDTO giftCertDTO = new GiftCertDTO(7, "mango", "GiftCard", null, BigDecimal.valueOf(100),
				30, null, null, null, null);

		when(giftCertService.findGiftCertById(7)).thenReturn(giftCertDTO);

		mockMvc.perform(get("/api/giftcerts/{id}", 7))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.price").value(100.0))
				.andExpect(jsonPath("$.id").value(7))
				.andExpect(jsonPath("$.owner").value("mango"))
				.andExpect(jsonPath("$.giftCertName").value("GiftCard"))
				.andExpect(jsonPath("$.duration").value(30));
	}

	@Test
	@WithMockUser
	void findAllTest() throws Exception {
		when(giftCertService.findAllGiftCert()).thenReturn(new ArrayList<>());

		mockMvc.perform(get("/api/giftcerts"))
				.andExpect(status().isOk());

	}

	@Test
	@WithMockUser
	void findGiftCertsByCustomerId() throws Exception {
		List<GiftCertDTO> response = new ArrayList<>();
		response.add(new GiftCertDTO());
		response.add(new GiftCertDTO());

		when(giftCertService.findGiftCertByCustomerId(1)).thenReturn(response);

		mockMvc.perform(get("/api/giftcerts/customer/{id}", 1))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0]").exists())
				.andExpect(jsonPath("$[1]").exists())
				.andExpect(jsonPath("$[2]").doesNotExist());

	}

	@Test
	@WithMockUser
	void searchGiftCertsTest() throws Exception {
		List<GiftCertDTO> response = new ArrayList<>();
		response.add(new GiftCertDTO());
		response.add(new GiftCertDTO());

		when(giftCertService.searchGiftCerts(any(), anyString(), anyString(), anyString(), anyBoolean()))
				.thenReturn(response);

		mockMvc.perform(get("/api/giftcerts/search")
						.param("name", "Gift")
						.param("description", "testing search")
						.param("tag", "TEST")
						.param("sortColumn", "gift"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0]").exists())
				.andExpect(jsonPath("$[1]").exists())
				.andExpect(jsonPath("$[2]").doesNotExist());
	}

	@Test
	@WithMockUser
	void searchWithoutParamsShouldThrowTest() throws Exception{
		mockMvc.perform(get("/api/giftcerts/search"))
				.andExpect(status().isBadRequest());
	}

	@Test
	@WithMockUser
	void modifyGiftCertTest() throws Exception {
		StringBuilder requestBody = new StringBuilder("{\"owner\":\"pineapple\",");
		requestBody.append("\"giftCertName\":\"GiftCard\",")
				.append("\"price\":100,")
				.append("\"duration\":30").append("}");

		GiftCertDTO giftCertDTO = new GiftCertDTO(8, "pineapple", "GiftCard", null, BigDecimal.valueOf(100),
				30, null, null, null, null);

		when(giftCertService.findGiftCertById(8)).thenReturn(giftCertDTO);

		mockMvc.perform(patch("/api/giftcerts/{id}", 8).with(csrf().asHeader())
						.contentType(MediaType.APPLICATION_JSON)
						.content(requestBody.toString()))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(8))
				.andExpect(jsonPath("$.owner").value("pineapple"))
				.andExpect(jsonPath("$.giftCertName").value("GiftCard"))
				.andExpect(jsonPath("$.price").value(100.0))
				.andExpect(jsonPath("$.duration").value(30));
	}

	@Test
	@WithMockUser
	void modifyWithEmptyInputShouldthrowTest() throws Exception {
		mockMvc.perform(patch("/api/giftcerts/{id}", 72).with(csrf().asHeader())
				.contentType(MediaType.APPLICATION_JSON)
				.content("{}"))
				.andExpect(status().isBadRequest());
	}

	@Test
	@WithMockUser
	void addTagToGiftCertTest() throws Exception {
		List<Tag> tags = new ArrayList<>();
		tags.add(new Tag(1, "TEST", null));

		GiftCertDTO giftCertDTO = new GiftCertDTO(9, "pineapple", "GiftCard", null, BigDecimal.valueOf(100),
				30, null, null, tags, null);

		when(giftCertService.findGiftCertById(9)).thenReturn(giftCertDTO);

		mockMvc.perform(patch("/api/giftcerts/{id}/tags", 9).with(csrf().asHeader())
				.param("tags", "TEST"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(9))
				.andExpect(jsonPath("$.owner").value("pineapple"))
				.andExpect(jsonPath("$.giftCertName").value("GiftCard"))
				.andExpect(jsonPath("$.price").value(100.0))
				.andExpect(jsonPath("$.duration").value(30))
				.andExpect(jsonPath("$.tags[0].tagName").value("TEST"));
	}

	@Test
	@WithMockUser
	void addEmptyTagToGiftCertShouldThrowTest() throws Exception {
		mockMvc.perform(patch("/api/giftcerts/{id}/tags", 4).with(csrf().asHeader())
						.param("tags", ""))
				.andExpect(status().isBadRequest());
	}

	@Test
	@WithMockUser
	void deleteTagTest() throws Exception {
		when(giftCertService.removeTagFromGiftCert(3, "TEST")).thenReturn(1);

		mockMvc.perform(delete("/api/giftcerts/{id}/tags", 3).with(csrf().asHeader())
						.param("tag", "TEST"))
				.andExpect(status().isNoContent());
	}

	@Test
	@WithMockUser
	void deleteTagFromNonExistingGiftCertTest() throws Exception {
		when(giftCertService.removeTagFromGiftCert(2, "TEST")).thenReturn(0);

		mockMvc.perform(delete("/api/giftcerts/{id}/tags", 2).with(csrf().asHeader())
						.param("tag", "TEST"))
				.andExpect(status().isNotFound());
	}

	@Test
	@WithMockUser
	void deleteGiftCertTest() throws Exception {
		when(giftCertService.deleteGiftCert(1)).thenReturn(1);

		mockMvc.perform(delete("/api/giftcerts/{id}", 1).with(csrf().asHeader()))
				.andExpect(status().isNoContent());
	}

	@Test
	@WithMockUser
	void deleteNonExistingGiftCertTest() throws Exception {
		when(giftCertService.deleteGiftCert(0)).thenReturn(0);

		mockMvc.perform(delete("/api/giftcerts/{id}", 0).with(csrf().asHeader()))
				.andExpect(status().isNotFound());
	}



}
